package MainClass;

import java.awt.image.SampleModel;
import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.ResultSet;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;

import java.io.IOException;
import java.io.Reader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application{

    static final String JDBC_DRIVER = "org.h2.Driver";
    static final String DB_URL = "jdbc:h2:~/test";
    static final String USER = "sa";
    static final String PASS = "";
    static String NameState = "";

    public static void main (String[] args) throws IOException {

        String input;
        Scanner sc = new Scanner(System.in);
        System.out.println("Input '0' to Create and insert new Data. Input '1' to continue when you have data in database");
        input = sc.nextLine();

        if (input.equals("0")){
            CreateTableData();
            InsertData();
        }

        if (input.equals("1")) {
            ArrayList<String> listName = SelectName();
            String inputNameIndex;
            for (int i = 0; i < listName.size(); i++) {
                String str = String.format("Index: %1$s - State: %2$s ", i, listName.get(i));
                System.out.println(str);
            }

            System.out.print("========================");
            System.out.print("Input Index State: ");
            Scanner c = new Scanner(System.in);
            inputNameIndex = c.nextLine();

            NameState = listName.get(Integer.parseInt(inputNameIndex));
            Application.launch(args);
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {

        ArrayList<CSVObject> obj = FetchData(NameState);

        CategoryAxis xAxis = new CategoryAxis();
        xAxis.setLabel(obj.get(0).get_state_or_territory());

        NumberAxis yAxis = new NumberAxis();
        yAxis.setLabel("Case");

        BarChart<String, Number> barChart = new BarChart<String, Number>(xAxis, yAxis);

        XYChart.Series<String, Number> dataSeries1 = new XYChart.Series<String, Number>();
        dataSeries1.setName("Affected case");

        for (int i = 0; i< obj.size(); i++) {
            dataSeries1.getData().add(new XYChart.Data<String, Number>(obj.get(i).get_time(), Integer.parseInt(obj.get(i).get_travel_associated_cases())));
        }

        barChart.getData().add(dataSeries1);

        barChart.setTitle(obj.get(0).get_state_or_territory());

        VBox vbox = new VBox(barChart);

        primaryStage.setTitle("Chart Infect Case in " + obj.get(0).get_state_or_territory());
        Scene scene = new Scene(vbox, 400, 200);

        primaryStage.setScene(scene);
        primaryStage.setHeight(500);
        primaryStage.setWidth(1200);

        primaryStage.show();
    }

    public static void InsertData() throws IOException{
        Connection conn = null;
        Statement stmt = null;

        ArrayList<String> file = InputFile();
        ArrayList<CSVObject> obj;

        try{
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);
            System.out.println("Connected database successfully...");

            // STEP 3: Execute a query
            stmt = conn.createStatement();
            String sql = "";
            for(int i = 0; i < file.size(); i++) {
                obj = ReadDataFromExcel(file.get(i));
                for (int j = 0; j < obj.size(); j++) {
                    Pattern pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
                    Matcher matcher = pattern.matcher(file.get(i));
                    String time = "";
                    if (matcher.find()) {
                        time = matcher.group();
                    }
                    else {
                        time = "lastest";
                    }
                    String str = String.format("INSERT INTO TEST (STATE_OR_TERRITORY ,TRAVEL_ASSOCIATED_CASES ,LOCALLY_ACQUIRED_CASES, TIME)" +
                                    "VALUES ('%1$s', '%2$s', '%3$s', '%4$s'); ",
                            obj.get(j).get_state_or_territory(),
                            obj.get(j).get_travel_associated_cases(),
                            obj.get(j).get_locally_acquired_cases(),
                            time);
                    sql = new StringBuilder(sql).append(str).toString();
                }
            }
            System.out.println("Executing...");
            stmt.executeUpdate(sql);
            System.out.println("Inserted records into the table...");

            // STEP 4: Clean-up environment
            stmt.close();
            conn.close();
        } catch(SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch(Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if(stmt!=null) stmt.close();
            } catch(SQLException se2) {
            } // nothing we can do
            try {
                if(conn!=null) conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            } // end finally try
        } // end try
        System.out.println("Goodbye!");
    }

    public static void CreateTableData() {
        Connection conn = null;
        Statement stmt = null;
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            //STEP 2: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            //STEP 3: Execute a query
            System.out.println("Creating table in given database...");
            stmt = conn.createStatement();
            String sql =  "DROP TABLE IF EXISTS TEST; " +
                    "CREATE TABLE TEST " +
                    "(id INTEGER not NULL AUTO_INCREMENT, " +
                    " state_or_territory VARCHAR(255), " +
                    " travel_associated_cases VARCHAR(255), " +
                    " locally_acquired_cases VARCHAR(255), " +
                    " time VARCHAR(255), " +
                    " PRIMARY KEY ( id ))";
            stmt.executeUpdate(sql);
            System.out.println("Created table in given database...");

            // STEP 4: Clean-up environment
            stmt.close();
            conn.close();
        } catch(SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        } catch(Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try{
                if(stmt!=null) stmt.close();
            } catch(SQLException se2) {
            } // nothing we can do
            try {
                if(conn!=null) conn.close();
            } catch(SQLException se){
                se.printStackTrace();
            } //end finally try
        } //end try
        System.out.println("End Session!");
    }

    public static ArrayList InputFile() {
        String dirPath;
        Scanner sc = new Scanner(System.in);
        System.out.print("Input folder path: ");

        dirPath = sc.nextLine();

        File f = new File(dirPath);
        File[] files = f.listFiles();
        ArrayList<String> obj = new ArrayList<String>();
        if (files != null) {
            for (int i = 0; i < files.length; i++) {
                if (files[i].toString().contains(".csv")) {
                    obj.add(files[i].toString());
                }
            }
            return obj;
        }
        return null;
    }

    public static ArrayList<CSVObject> ReadDataFromExcel(String SAMPLE_CSV_FILE_PATH) throws IOException {
        try (
                Reader reader = Files.newBufferedReader(Paths.get(SAMPLE_CSV_FILE_PATH));
                CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();

        ) {
            ArrayList<CSVObject> obj = new ArrayList<CSVObject>();

            // Reading Records One by One in a String array
            List<String[]> records = csvReader.readAll();
            for (String[] record : records) {
                CSVObject csv = new CSVObject(record[0], record[1], record[2], "0");
                obj.add(csv);
                System.out.println("File path: " + SAMPLE_CSV_FILE_PATH);
                System.out.println("state_or_territory : " + record[0]);
                System.out.println("travel_associated_cases : " + record[1]);
                System.out.println("locally_acquired_cases : " + record[2]);
                System.out.println("---------------------------");
            }
            return obj;
        }
    }

    public static ArrayList<String> SelectName(){
        Connection conn = null;
        Statement stmt = null;
        ArrayList<String> listName = new ArrayList<String>();
        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL,USER,PASS);

            // STEP 3: Execute a query
            System.out.println("Connected database successfully...");
            stmt = conn.createStatement();
            String sql = "SELECT STATE_OR_TERRITORY \n" +
                    "FROM TEST\n" +
                    "\n" +
                    "GROUP BY STATE_OR_TERRITORY \n" +
                    "ORDER BY STATE_OR_TERRITORY ";
            System.out.println("Executing...");
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 4: Extract data from result set
            while(rs.next()) {
                // Retrieve by column name
                String name = rs.getString("STATE_OR_TERRITORY");
                listName.add(name);
            }
            // STEP 5: Clean-up environment
            rs.close();
        } catch(SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch(Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if(stmt!=null) stmt.close();
            } catch(SQLException se2) {
            } // nothing we can do
            try {
                if(conn!=null) conn.close();
            } catch(SQLException se) {
                se.printStackTrace();
            } // end finally try
            return listName;
        } // end try
    }

    public static ArrayList<CSVObject> FetchData(String nameState) {
        Connection conn = null;
        Statement stmt = null;
        ArrayList<CSVObject> obj = new ArrayList<CSVObject>();

        try {
            // STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);

            // STEP 2: Open a connection
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);

            // STEP 3: Execute a query
            System.out.println("Connected database successfully...");
            stmt = conn.createStatement();
            String sql = "SELECT * \n" +
                    "from TEST\n" +
                    "where STATE_OR_TERRITORY = '" + nameState + "'";

            System.out.println("Executing...");
            ResultSet rs = stmt.executeQuery(sql);

            // STEP 4: Extract data from result set
            while (rs.next()) {
                // Retrieve by column name
                String state_or_territory = rs.getString("STATE_OR_TERRITORY");
                String travel_associated_cases = rs.getString("TRAVEL_ASSOCIATED_CASES");
                String locally_acquired_cases = rs.getString("LOCALLY_ACQUIRED_CASES");
                String time = rs.getString("TIME");

                CSVObject csv = new CSVObject(
                        state_or_territory,
                        travel_associated_cases,
                        locally_acquired_cases,
                        time);
                obj.add(csv);
            }
            // STEP 5: Clean-up environment
            rs.close();

//            for (int i = 0; i < obj.size(); i++) {
//                System.out.println(obj.get(i).get_state_or_territory());
//                System.out.println(obj.get(i).get_travel_associated_cases());
//                System.out.println(obj.get(i).get_locally_acquired_cases());
//                System.out.println(obj.get(i).get_time());
//            }

        } catch (SQLException se) {
            // Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
            // Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            // finally block used to close resources
            try {
                if (stmt != null) stmt.close();
            } catch (SQLException se2) {
            } // nothing we can do
            try {
                if (conn != null) conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            } // end finally try
            return obj;
        } // end try
    }
}



